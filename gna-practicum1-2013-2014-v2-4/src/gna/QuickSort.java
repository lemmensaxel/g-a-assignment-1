package gna;

/**
 * Performs sort by using the Selection Sort algorithm.
 * 
 * @author fill in your name here
 */
public class QuickSort implements libpract.SortingAlgorithm {

	private static int compares;

	/**
	 * Sorts the given array using quick sort.
	 * 
	 * @return The number of comparisons (i.e. calls to compareTo) performed by
	 *         the algorithm.
	 */
	
	// VARIANT 2
	public <T extends Comparable<T>> int sort2(T[] array) {
		compares = 0;
		sort2(array, 0, array.length - 1);
		return compares;
	}

	public <T extends Comparable<T>> void sort2(T[] array, int lo, int hi) {
		if (hi <= lo)
			return;
		int lt = lo;
		int i = lo+1;
		int gt = hi;
		T v = array[lo];
		while (i <= gt) {
			int cmp = array[i].compareTo(v);
			compares++;
			if (cmp < 0)
				exch(array, lt++, i++);
			else if (cmp > 0)
				exch(array, i, gt--);
			else
				i++;
		}
		sort2(array, lo, lt-1);
		sort2(array, gt+1, hi);
	}
	
	// VARIANT 3
	public <T extends Comparable<T>> int sort3(T[] array) {
		compares = 0;
		sort3(array, 0, array.length - 1);
		return compares;
	}

	public <T extends Comparable<T>> void sort3(T[] array, int lo, int hi) {
		if (hi <= lo)
			return;

		int j = partition3(array, lo, hi);

		sort3(array, lo, j - 1);
		sort3(array, j + 1, hi);
	}

	// VARIANT 1
	public <T extends Comparable<T>> int sort(T[] array) {
		compares = 0;
		sort(array, 0, array.length - 1);
		return compares;
	}

	public <T extends Comparable<T>> void sort(T[] array, int lo, int hi) {
		if (hi <= lo)
			return;

		int j = partition1(array, lo, hi);

		sort(array, lo, j - 1);
		sort(array, j + 1, hi);

	}

	// PARTITION VARIANT 1
	private static int partition1(Comparable[] row, int lo, int hi) {
		int i = lo;
		int j = hi + 1;

		Comparable v = row[lo];

		while (true) {
			while (less(row[++i], v))
				if (i == hi)
					break;
			while (less(v, row[--j]))
				if (j == lo)
					break;
			if (i >= j)
				break;
			exch(row, i, j);
		}

		exch(row, lo, j);
		return j;
	}

	// PARTITION VARIANT 3 (=Lomuto)
	private static int partition3(Comparable[] row, int lo, int hi) {
		int i = lo - 1;
		Comparable v = row[hi];
		for (int j = lo; j <= hi - 1; j++) {
			if (less(row[j], v)) {
				i++;
				exch(row, i, j);
			}
		}
		exch(row, i + 1, hi);
		return i + 1;
	}

	private static boolean less(Comparable a, Comparable b) {
		compares++;
		return a.compareTo(b) < 0;
	}

	private static void exch(Comparable[] row, int i, int j) {
		Comparable t = row[i];
		row[i] = row[j];
		row[j] = t;

	}

	/**
	 * Constructor.
	 */
	public QuickSort() {
	}
}
