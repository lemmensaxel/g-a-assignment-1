package gna;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Random;

public class Main {
	/**
	 * Example main function.
	 * 
	 * You can replace this.
	 * @throws FileNotFoundException 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) throws FileNotFoundException {
		StopWatch stopwatch = new StopWatch();
		for (int k = 0; k < 25; k++) {
		//Creating objects
		// result(i)(t)
		InsertionSort insertionsort = new InsertionSort();
		SelectionSort selectionsort = new SelectionSort();
		QuickSort quicksort = new QuickSort();
		PrintWriter writer = new PrintWriter("results/selectionsort/exp/results_selection_try_" + k +".dat");
		PrintWriter writer2 = new PrintWriter("results/insertionsort/exp/results_insert_try_" + k +".dat");
		PrintWriter writer3 = new PrintWriter("results/quicksort/var1/exp/results_quick1_try_" + k +".dat");
		PrintWriter writer4 = new PrintWriter("results/quicksort/var2/exp/results_quick2_try_" + k +".dat");
		PrintWriter writer5 = new PrintWriter("results/quicksort/var3/exp/results_quick3_try_" + k +".dat");	
		
		PrintWriter writer6 = new PrintWriter("results/selectionsort/dr/results_selection_dr_try_" + k +".dat");
		PrintWriter writer7 = new PrintWriter("results/insertionsort/dr/results_insert_try_dr_try_" + k +".dat");
		PrintWriter writer8 = new PrintWriter("results/quicksort/var1/dr/results_quick1_try_dr_try_" + k +".dat");
		PrintWriter writer9 = new PrintWriter("results/quicksort/var2/dr/results_quick2_try_dr_try_" + k +".dat");
		PrintWriter writer10 = new PrintWriter("results/quicksort/var3/dr/results_quick3_try_dr_try_" + k +".dat");	
		
		//Sorting the arrays
		for (int i = 1; i<=100; i++) {
			Comparable[] array = generateArray(i);
			Comparable[] array_selectionsort = array.clone();
			Comparable[] array_insertionsort = array.clone();
			Comparable[] array_quicksort1 = array.clone();
			Comparable[] array_quicksort2 = array.clone();
			Comparable[] array_quicksort3 = array.clone();
			writer.println(i + " " + selectionsort.sort(array_selectionsort));
			writer2.println(i + " " + insertionsort.sort(array_insertionsort));
			writer3.println(i + " " + quicksort.sort(array_quicksort1));
			writer4.println(i + " " + quicksort.sort2(array_quicksort2));
			writer5.println(i + " " + quicksort.sort3(array_quicksort3));
		}
		writer.println("\n");
		writer2.println("\n");
		writer3.println("\n");
		writer4.println("\n");
		writer5.println("\n");

		
		//Doubling ratio experiment
		stopwatch.start();
		for (int i = 100; i <  51300; i*=2) {
			Comparable[] array = generateArray(i);
			Comparable[] array_insertionsort = array.clone();
			Comparable[] array_quicksort1 = array.clone();
			writer7.println(i + " " + insertionsort.sort(array_insertionsort));
			writer8.println(i + " " + quicksort.sort(array_quicksort1));
		}
		writer.println();
		
		writer.close();
		writer2.close();
		writer3.close();
		writer4.close();
		writer5.close();
		writer6.close();
		writer7.close();
		writer8.close();
		writer9.close();
		writer10.close();
		}
		System.out.println(stopwatch.getElapsedTimeinNanoSeconds());
	}

	/**
	 * A method that generates arrays of specific dimension, filled with integers
	 * between 0 and 1000.
	 * 
	 * @param dimension
	 */
	public static Comparable[] generateArray(int dimension) {
		Random randomizer = new Random();
		Comparable[] array = new Comparable[dimension];

		for (int i = 0; i < array.length; i++) {
			array[i] = randomizer.nextInt();
		}
		return array;
	}
	
	
	/**
	 * A method that checks if a given array is sorted
	 * @param array
	 */
	public static boolean isSorted(Comparable[] array) {
		for (int i = 1; i < array.length; i++) {
			if (less(array[i], array[i - 1])) {
				return false;
			}
		}
		return true;
	}
	
	
	/**
	 * A method that returns true if a < b
	 * @param a
	 * @param b
	 */
	private static boolean less(Comparable a, Comparable b) {
		return a.compareTo(b) < 0;
	}
	

	/**
	 * A method that prints the given array.
	 * @param array
	 */
	private static void show(Comparable[] array) {
		System.out.println(Arrays.toString(array));
	}
}
