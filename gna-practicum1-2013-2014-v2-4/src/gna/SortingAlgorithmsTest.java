package gna;

import static org.junit.Assert.*;

import java.util.Arrays;

import libpract.SortingAlgorithm;

import org.junit.Test;

/**
 * Tests for SortingAlgorithms.
 */
public class SortingAlgorithmsTest {
  
  
	@Test
	public void InsertionSort_test() {
		Comparable[] row = {2, 8, 90, 76, 54, 3, 8, 456, 9876543, 67, 23};
		Comparable[] sortedRow = {2, 3, 8, 8, 23, 54, 67, 76, 90, 456, 9876543};
		InsertionSort sorter = new InsertionSort();
		sorter.sort(row);
	
		assertTrue(Arrays.equals(row, sortedRow));
	}

	@Test
	public void SelectionSort_test() {
		Comparable[] row = {2, 8, 90, 76, 54, 3, 8, 456, 9876543, 67, 23};
		Comparable[] sortedRow = {2, 3, 8, 8, 23, 54, 67, 76, 90, 456, 9876543};
		SelectionSort sorter = new SelectionSort();
		sorter.sort(row);
	
		assertTrue(Arrays.equals(row, sortedRow));
	}
	
	@Test
	public void QuickSort_sort_test() {
		Comparable[] row = {2, 8, 90, 76, 54, 3, 8, 456, 9876543, 67, 23};
		Comparable[] sortedRow = {2, 3, 8, 8, 23, 54, 67, 76, 90, 456, 9876543};
		QuickSort sorter = new QuickSort();
		sorter.sort(row);
	
		assertTrue(Arrays.equals(row, sortedRow));
	}
	
	@Test
	public void QuickSort_sort2_test() {
		Comparable[] row = {2, 8, 90, 76, 54, 3, 8, 456, 9876543, 67, 23};
		Comparable[] sortedRow = {2, 3, 8, 8, 23, 54, 67, 76, 90, 456, 9876543};
		QuickSort sorter = new QuickSort();
		sorter.sort2(row);
	
		assertTrue(Arrays.equals(row, sortedRow));
	}
	
	@Test
	public void QuickSort_sort3_test() {
		Comparable[] row = {2, 8, 90, 76, 54, 3, 8, 456, 9876543, 67, 23};
		Comparable[] sortedRow = {2, 3, 8, 8, 23, 54, 67, 76, 90, 456, 9876543};
		QuickSort sorter = new QuickSort();
		sorter.sort3(row);
	
		assertTrue(Arrays.equals(row, sortedRow));
	}
  
}
