package gna;

/**
 * Performs sort by using the Selection Sort algorithm.
 * 
 * @author fill in your name here
 */
public class InsertionSort implements libpract.SortingAlgorithm {
	/**
	 * Sorts the given array using insertion sort.
	 * 
	 * @return The number of comparisons (i.e. calls to compareTo) performed by
	 *         the algorithm.
	 */

	public static int compares;

	public <T extends Comparable<T>> int sort(T[] array) {
		compares = 0;

		int N = array.length;

		for (int i = 1; i < N; i++) {
			for (int j = i; j > 0 && less(array[j], array[j - 1]); j--)
				exch(array, j, j - 1);
		}

		return compares;
	}

	private static boolean less(Comparable a, Comparable b) {
		compares++;
		return a.compareTo(b) < 0;
	}

	private static void exch(Comparable[] row, int i, int j) {
		Comparable t = row[i];
		row[i] = row[j];
		row[j] = t;

	}

	/**
	 * Constructor.
	 */
	public InsertionSort() {
	}
}
