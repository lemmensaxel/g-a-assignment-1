package gna;

/**
 * Performs sort by using the Selection Sort algorithm.
 * 
 * @author fill in your name here
 */
public class SelectionSort implements libpract.SortingAlgorithm {
	public static int compares;

	/**
	 * Sorts the given array using selection sort.
	 * 
	 * @return The number of comparisons (i.e. calls to compareTo) performed by
	 *         the algorithm.
	 */
	public <T extends Comparable<T>> int sort(T[] array) {
		compares = 0;
		int N = array.length;
		for (int i = 0; i < N; i++) {
			int min = i;
			for (int j = i + 1; j < N; j++) {
				if (less(array[j], array[min])) {
					min = j;
				}
			}
			exch(array, i, min);

		}

		return compares;

	}

	private static boolean less(Comparable a, Comparable b) {
		compares++;
		return a.compareTo(b) < 0;
	}

	private static void exch(Comparable[] row, int i, int j) {
		Comparable t = row[i];
		row[i] = row[j];
		row[j] = t;

	}

	/**
	 * Constructor.
	 */
	public SelectionSort() {
	}
}
