\select@language {dutch}
\contentsline {section}{\numberline {1}Inleiding}{2}
\contentsline {section}{\numberline {2}Selectionsort}{3}
\contentsline {subsection}{\numberline {2.1}Het algoritme}{3}
\contentsline {subsection}{\numberline {2.2}De theoretische afleiding van het aantal compares}{3}
\contentsline {subsection}{\numberline {2.3}De bekomen resultaten}{4}
\contentsline {subsubsection}{\numberline {2.3.1}Experiment 1}{4}
\contentsline {subsection}{\numberline {2.4}Conclusie}{4}
\contentsline {section}{\numberline {3}Insertionsort}{5}
\contentsline {subsection}{\numberline {3.1}Het algoritme}{5}
\contentsline {subsection}{\numberline {3.2}De theoretische afleiding}{5}
\contentsline {subsection}{\numberline {3.3}De bekomen resultaten}{6}
\contentsline {subsubsection}{\numberline {3.3.1}Experiment 1}{6}
\contentsline {subsubsection}{\numberline {3.3.2}Experiment 2}{7}
\contentsline {subsection}{\numberline {3.4}Conclusie}{7}
\contentsline {section}{\numberline {4}Quicksort}{8}
\contentsline {subsection}{\numberline {4.1}Basis quicksort variant}{8}
\contentsline {subsubsection}{\numberline {4.1.1}Het algoritme}{8}
\contentsline {subsubsection}{\numberline {4.1.2}De theoretische afleiding}{8}
\contentsline {subsubsection}{\numberline {4.1.3}De bekomen resultaten - Experiment 1}{9}
\contentsline {subsubsection}{\numberline {4.1.4}De bekomen resultaten - Experiment 2}{10}
\contentsline {subsubsection}{\numberline {4.1.5}Conclusie}{10}
\contentsline {subsection}{\numberline {4.2}Andere varianten van quicksort}{11}
\contentsline {subsubsection}{\numberline {4.2.1}Variant 1- 3-wegs partitionering}{11}
\contentsline {subsubsection}{\numberline {4.2.2}Variant 2 - Lomuto}{12}
\contentsline {section}{\numberline {5}Conclusie}{13}
